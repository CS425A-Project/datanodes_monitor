import subprocess
import random
import string
import time, threading
import socket
import sys
import transaction
from ZODB import FileStorage, DB
import datetime

space_left = []
node_list = []
interval = 3 # In sec
db_file_path = ""
debug = False

def init_connection(database_file_path):
	storage = FileStorage.FileStorage(database_file_path, read_only=True)
	db = DB(storage)
	connection = db.open()
	return connection.root()

def cron_job(botnet_array):
	global node_list
	node_list = []
	for address in botnet_array:
		response = send_data('status', address, 12346)
		if response is not None:
			space_left.append(response)
			node_list.append(address)
	node_list = [x for (y,x) in sorted(zip(space_left,node_list))] 
	if debug:
		print(node_list)
	botnet_array = init_connection(db_file_path)['ip_list']
	threading.Timer(interval, cron_job, [botnet_array]).start()
def get_ipv4_addr(host, port):
    addr = socket.getaddrinfo(host, port, socket.AF_INET, 0, socket.SOL_TCP)
    if len(addr) == 0:
    	raise Exception("There is no IPv4 address configured for host: "+host)
    return addr[0][-1]

def send_data(data, host, port):
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	try:
		s.connect(get_ipv4_addr(host, port))
		s.send(data.encode())
		response = s.recv(1024)
		s.close()
		return response.decode()
	except:
		return None

def init_monitor(db_path):
	global db_file_path
	db_file_path = db_path
	ip_list = init_connection(db_file_path)
	if 'ip_list' not in ip_list.keys():
		print('Error: Database empty.')
		exit(0)
	cron_job(ip_list['ip_list'])

def get_max_three_datanodes():
	return {
	'ip':  node_list[0:3],
	'port': 12347,
	'filename': random.choice(string.ascii_letters) + "".join([random.choice(string.ascii_letters+'._') for x in range(10)]) + "".join(str(datetime.datetime.now()).replace('-', ' ').replace(':', ' ').split())
	}

def get_a_datanode():
	return {
	'ip':  node_list[0],
	'port': 12347,
	'filename': random.choice(string.ascii_letters) + "".join([random.choice(string.ascii_letters+'._') for x in range(10)]) + "".join(str(datetime.datetime.now()).replace('-', ' ').replace(':', ' ').split())
	}


if __name__ == '__main__':
	global debug
	debug = True
	init_monitor(sys.argv[1])
	print(get_a_datanode())
