import socket
import atexit
import sys
import json
import transaction
from ZODB import FileStorage, DB
import sys

database_file_path = 'ip_list.db'

def init_database():
	storage = FileStorage.FileStorage(database_file_path)
	db = DB(storage)
	connection = db.open()
	ip_list = connection.root()
	return ip_list

def init_table(ip_list):
	ip_list['ip_list'] = []
	transaction.commit()

def insert_info(ip_list, ip_addr):
	if ip_addr not in ip_list['ip_list']:
		ip_list['ip_list'].append(ip_addr)
		ip_list._p_changed = 1
		transaction.commit()

def cleanup(sock):
    print("Closing current socket...")
    sock.close()
    print("Bye.")

def get_ipv4_addr(host, port):
    addr = socket.getaddrinfo(host, port, socket.AF_INET, 0, socket.SOL_TCP)
    if len(addr) == 0:
    	raise Exception("There is no IPv4 address configured for host: "+host)
    return addr[0][-1]

def start_server(ip_list, sock_addr):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(sock_addr)
    s.listen(1024)
    atexit.register(cleanup, s)
    process_requests(ip_list, s)


def process_requests(ip_list, s):
	while True:
		conn, addr = s.accept()
		raw_data = conn.recv(1024)
		if raw_data != bytes():
			print(raw_data)
			insert_info(
				ip_list,
				raw_data.decode()
				)
			conn.send('OK'.encode())

def init_ip_list(init=False):
	ip_list = init_database()
	if init:
		init_table(ip_list)
	return ip_list

if __name__ == '__main__':
	host='127.0.0.1'
	port=12345
	init_arg = 1
	if len(sys.argv) > 2:
		host = sys.argv[1]
		init_arg = init_arg+1
	elif len(sys.argv) > 3:
		host = sys.argv[1]
		port = int(sys.argv[2])
		init_arg = init_arg +1
	if len(sys.argv) == init_arg +1:
		if sys.argv[init_arg] == 'init':
			ip_list = init_ip_list(True)
		elif sys.argv[init_arg] == 'restore':
			ip_list = init_ip_list(False)
		else:
			print("Invalid arguments.")
			exit(0)
	else:
		print("Invalid arguments.")
		exit(0)

	print("Creating a server with host '" + host + "' and port " + str(port) + "...")
	sock_addr = get_ipv4_addr(host, port)
	start_server(ip_list, sock_addr)